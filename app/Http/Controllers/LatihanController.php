<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function perkenalan(){

        // passing variabel to view
        $nama = "Adila";
        $alamat = "Bandung";
        $umur = 16;


        return view('pages.perkenalan', compact('nama','alamat','umur'));
    }

    public function intro($nama,$alamat,$umur){

        // passing variabel to view
        
        

        return view('pages.introduction', compact('nama','alamat','umur'));
    }

    public function siswa(){

        $a = [
            array('id' => 1,'name' => 'Dadang','age' => 15,'hobi' => [
                'mancing','futsal','renang',
            ]),

            array('id' => 2,'name' => 'Dudung','age' => 18,'hobi' => [
                'baca buku','bernyanyi',
            ]),
        ];
        
        
        // dd($a)
        return view('pages.siswa',compact('a'));
    }



    public function skripsi(){

        $dosen= [
           ['name' => 'Yusuf', 'matkul' => 'pemrograman mobile', 'mahasiswa' =>
            [

                ['name' => 'Muhammad Sholeh','nilai' => 78],

                ['name' => 'Jujun Junaedi','nilai' => 85],

                ['name' => 'Mamat Alkatiri','nilai' => 90],
           ],
           ],

           ['name' => 'Yaris', 'matkul' => 'pemrograman mobile', 'mahasiswa' => 
           [
                ['name' => 'Alfred','nilai' => 67],

                ['name' => 'Bruno','nilai' => 85],
           ],
           ],
            
        ];
       
        
        
        // dd($a)
        return view('pages.dosen',compact('dosen'));
    }



    public function penyiaran(){

        $daftar= [
           
            array('tv' => 'net tv','acara' => 'talk show','tayang' => 'hari senin jam 10.00 wib','tgl' => '01 juli 2022'),

            array('tv' => 'tvri','acara' => 'berita terkini','tayang' => 'hari selasa jam 09.00 wib','tgl' => '02 juli 2022'),

            array('tv' => 'beinsport','acara' => 'sepak bola','tayang' => 'hari rabu jam 19.00 wib','tgl' => '03 juli 2022'),

            array('tv' => 'ochannel','acara' => 'wacana','tayang' => 'hari kamis jam 13.00 wib','tgl' => '04 juli 2022'),

            array('tv' => 'indosiar','acara' => 'dangdut academy','tayang' => 'hari senin jam 20.00 wib','tgl' => '05 juli 2022'),
        ];
       
        
        
        // dd($a)
        return view('pages.siarantv',compact('daftar'));
    }


    public function jalanjalan(){

        $merk= [
            ['name' => 'Alfian','membeli' =>
             [
 
                 ['beli' => 'sepatu','merkk' => 'vans','harga' => 250000],
 
                 ['beli' => 'baju','merkk' => 'zara','harga' => 100000],
 
                 ['beli' => 'celana','merkk' => 'dior','harga' => 150000],

                 ['beli' => 'kupluk','merkk' => 'vans','harga' => 100000],
            ],
            ],
 
            ['name' => 'Dida', 'membeli' => 
            [
                 ['beli' => 'topi','merkk' => 'erigo','harga' => 100000],
 
                 ['beli' => 'baju','merkk' => '3seconds','harga' => 75000],

                 ['beli' => 'celana','merkk' => 'h & m','harga' => 125000],
            ],
            ],
             
         ];

         // dd($a)
        return view('pages.toko',compact('merk'));
    }
    

    public function sekolah(){

        $data= [
        ['jurusan' => 'TKR','nilai' =>

            [
                ['nama' => 'Agus','indo'=>80, 'inggris'=>97,'produktif'=>67,'mtk'=>100, ],

                ['nama' => 'Mahmud','indo'=>78, 'inggris'=>86,'produktif'=>90,'mtk'=>67, ],

            ], 
        ],


        ['jurusan' => 'TSM','nilai' =>

            [
                ['nama' => 'Rendi','indo'=>90, 'inggris'=>50,'produktif'=>65,'mtk'=>78, ],


            ], 
        ],

        ['jurusan' => 'RPL','nilai' =>

            [

                ['nama' => 'Firman','indo'=>78, 'inggris'=>90,'produktif'=>56,'mtk'=>78, ],

                ['nama' => 'Abdul','indo'=>89, 'inggris'=>67,'produktif'=>80,'mtk'=>90, ],

            ], 
        ],
        ];

         // dd($a)
        return view('pages.grade',compact('data'));
    }
}

