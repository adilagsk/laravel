<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Transaksi;
class TransaksiController extends Controller
{
    public function tampil(){


        //menampilkan semua data dari model post
        //model post mewakili data dari table post 'post'
        //atau bisa juga query -> select * from posts
        $beli = Transaksi::all();
        return view('beli.index',compact('beli'));
    }
}
