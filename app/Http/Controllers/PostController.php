<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;
// memanggil model Post

class PostController extends Controller
{
    public function tampil(){


        //menampilkan semua data dari model post
        //model post mewakili data dari table post 'post'
        //atau bisa juga query -> select * from posts
        $post = Post::all();
        return view('post.index',compact('post'));
    }

    public function search($id){

        // memcari data dari model berdasarkan id
        // query->select * from post where id = $id
        $post = Post::find($id);
        return $post;
    }


    public function search_title($title){

        // memcari data dari model berdasarkan id
        // query->select * from post where title like %title%
        $post = Post::where('title','like','%' . $title .'%')->get();
        return $post;
    }

    public function edit($id,$title,$content){


        // mengedit data post berdasarkan id
        $post = Post::find($id);
        $post->title = $a;
        $post->content = $b;
        $post->save();
        return $post;
    }

    public function tambah($a,$b){


        // menambah data post berdasarkan id
        $post = new Post();
        $post->title = $a;
        $post->content = $b;
        $post->save();
        return $post;
    }


    public function hapus($id){


        // menghapus data post berdasarkan id
        $post = Post::find($id);
        $post->delete();
        return redirect('post');
    }
}

