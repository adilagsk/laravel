<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LatihanController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route basic
Route::get('/about', function () {
    return view('tentang');
});

Route::get('profile', function () {
    $nama = "Adila";
    $ttl = "Sukabmi 15 agustus 2005";
    $kelas = "XI RPL 1";
    $umur = "15 Tahun";
    $alamat = "Tarate 2";

    // compact bertugas untuk mengirimkan var kedalam view
    return view('pages.biodata', compact('nama','ttl','kelas','umur','alamat'));
});

// route parameter
Route::get('biodata/{nama}', function ($a) {
    return view('pages.biodata1', compact('a'));
});



Route::get('order/{makanan}/{minuman}/{harga}', function ($makanan,$minuman,$harga) {
    return view('pages.beli', compact('makanan','minuman','harga'));
});


// route optional parameter
Route::get('pesan/{menu?}', function ($a ="-") {
    return view('pages.pesan', compact('a'));
});



Route::get('memesan/{menu?}/{menu1?}/{menu2?}', function ($a ="silahkan pesan terlebih dahulu",$b=null,$c=null) {

    
    return view('pages.pesanan', compact('a','b','c'));
});

// pemanggilan route dengan controller
Route::get('latihan', [LatihanController::class, 'perkenalan']);

Route::get('latihan/{nama}/{alamat}/{umur}', [LatihanController::class, 'intro']);


Route::get('data', [LatihanController::class, 'siswa']);
Route::get('latihan', [LatihanController::class, 'skripsi']);
Route::get('latihann', [LatihanController::class, 'penyiaran']);
Route::get('toko', [LatihanController::class, 'jalanjalan']);
Route::get('latihannn', [LatihanController::class, 'sekolah']);


//route post
Route::get('post', [PostController::class, 'tampil']);
Route::get('post/{id}', [PostController::class, 'search']);
Route::get('post/judul/{title}', [PostController::class, 'search_title']);
Route::get('post/edit/{id}/{title}/{content}', [PostController::class, 'edit']);
Route::get('post/tambah/{title}/{content}', [PostController::class, 'tambah']);
Route::get('post/delete/{id}', [PostController::class, 'hapus']);


//tugas
Route::get('sekolah', [SiswaController::class, 'tampil']);
//tugas
Route::get('sekolahh', [TransaksiController::class, 'tampil']);