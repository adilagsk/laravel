<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <center>
    <fieldset>
        <legend>Data Siswa</legend>
        <br>
        <table border=1>
            <tr>
                <th>Nomor</th>
                <th>Id</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Alamat</th>
                
            </tr>
            @php $no = 1; @endphp
            @foreach($sekolah as $dataa)

            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $dataa->id}}</td>
                <td>{{ $dataa['nama'] }}</td>
                <td>{{ $dataa->kelas }}</td>
                <td>{{ $dataa['alamat'] }}</td>
            </tr>
            @endforeach
        </table>
    </fieldset>
    </center>
</body>
</html>