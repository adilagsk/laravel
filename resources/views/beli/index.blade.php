<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<center>
    <fieldset>
        <legend>Tabel Transaksi</legend>
        <br>
        <table border=1>
            <tr>
                <th>Nomor</th>
                <th>Id</th>
                <th>Nama_Barang</th>
                <th>Nama_Pembeli</th>
                <th>Tanggal</th>
                <th>Keterangan</th>
                
            </tr>
            @php $no = 1; @endphp
            @foreach($beli as $dataa)

            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $dataa->id}}</td>
                <td>{{ $dataa['nama_barang'] }}</td>
                <td>{{ $dataa->nama_pembeli }}</td>
                <td>{{ $dataa['tanggal'] }}</td>
                <td>{{ $dataa['keterangan'] }}</td>
            </tr>
            @endforeach
        </table>
    </fieldset>
    </center>
</body>
</html>