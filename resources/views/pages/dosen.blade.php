<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>
            Data Dosen
        </legend>
        @foreach ($dosen as $data)
        
        @php
        $total = 0;
        @endphp

        Dosen : {{$data['name']}} <br>
        Mata Kuliah : {{$data['matkul']}} <br>
        Daftar Mahasiswa :

        @foreach($data['mahasiswa'] as $mhs)
        <li>{{$mhs['name']}} <br>
        Nilai : {{$mhs['nilai']}} 
        </li>

        <hr>
        @php $total += $mhs['nilai'] @endphp

        @endforeach

        @php $rata = $total /count($data['mahasiswa'])@endphp
        Total Nilai Mahasiswa : {{$total}} <br>
        Rata - rata nilai skripsi mahasiswa bimbingan <b>{{$data['name']}}</b>: 
        {{$rata}}

        <b>
            <hr style="border: 1px dashed purple">
        </b>
        @endforeach

        
    </fieldset>
</body>
</html>