<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Saya Memesan : </h1>


    @if ($a = null)
    {{$a}}

    @elseif ($a = 'mie goreng')
    Makanan : {{$a}}
    Harga : Rp. 5000

    @elseif ($a = 'seblak')
    Makanan : {{$a}}
    Harga : Rp. 7500

    @elseif ($a = 'nasi padang')
    Makanan : {{$a}}
    Harga : Rp. 15000

    @else
    Makanan : {{$a}}
    @endif
    <br>


    @if ($b = null)
    {{$b}}

    @elseif ($b = 'kopi')
    Minuman : {{$b}}
    Harga : Rp. 5000

    @elseif ($b = 'jus')
    Minuman : {{$b}}
    Harga : Rp. 7500

    @elseif ($b = 'teh')
    Minuman : {{$b}}
    Harga : Rp. 8500

    @else
    Minuman : {{$b}}
    @endif
    <br>


    @if ($c = null)
    {{$c}}

    @else
    Camilan : {{$c}}
    @endif
    <br>



</body>
</html>