<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>toko</title>
</head>
<body>
    <fieldset>
        <legend>Belanja di Toko  Yang Ada di Trunojoyo</legend>


        @foreach ($merk as $data)
        
        @php
        $total = 0;
        @endphp

        Nama : {{$data['name']}} <br>
        Daftar beli :
        
        @foreach($data['membeli'] as $dataa)
        <li>
            Membeli : {{$dataa['beli']}} <br>
            Merk : {{$dataa['merkk']}} <br>
            Harga  : Rp. {{$dataa['harga']}} <br>  
        </li>
        <hr>

        @php $total += $dataa['harga'] @endphp

        @endforeach
        Total Harga : Rp. {{$total}} <br>

        @if ($total > 500000 )
        @php $cashback = (10/100) * $total @endphp
        Mendapat cashback sebesar 10% <br>


        @elseif ($total < 500000)
        @php $cashback = (5/100) * $total @endphp
        Mendapat cashback sebesar 5% <br>

        @else
        tidak mendapat cashback
        @endif

       
        Cashback yang di dapat oleh  <b>{{$data['name']}}</b>: Rp.
        {{number_format($cashback)}},00  <br>

        Jadi jumlah yang harus dibayar oleh <b>{{$data['name']}}</b> sebesar : Rp. {{number_format($total - $cashback)}},00


        <b>
            <hr style="border: 1px dashed purple">
        </b>
        @endforeach
    </fieldset>
</body>
</html>