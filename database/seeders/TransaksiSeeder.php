<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [
            //transaksi
            ['nama_barang'=>'buku','nama_pembeli'=>'ulya','tanggal',
            'Keterangan'=> 'bebas'],

            //pembayaran
            ['tgl_barang'=>15,'total_bayar'=>50000,'kode_transaksi'=>123456],


             //supplier
            ['nama_suplier'=>'ai','no_telp'=>628654999,'alamat'=> 'tarate 2'],

            //barang
            ['nama_barang'=>'buku','harga'=>50000,'stok'=> 'ada','nama_supplier'=>'ai'],

            //pembeli
            ['nama_pembeli'=>'adila','jk'=>'perempuan','no_telp'=> 62976543, 'alamat'=>'tarate 5'],



            
        ];

        DB::table('transaksis')->insert($sample);
    }
}
