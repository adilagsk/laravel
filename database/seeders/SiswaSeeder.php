<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [
            ['nama'=>'adila','kelas'=> 'XII RPL 1','alamat'=>'Bandung'],

            ['nama'=>'ai','kelas'=> 'XII RPL 1','alamat'=>'Bandung'],

            ['nama'=>'gibral','kelas'=> 'XII RPL 1','alamat'=>'Bandung'],

            ['nama'=>'dida','kelas'=> 'XII RPL 2','alamat'=>'Bandung'],

            ['nama'=>'risma','kelas'=> 'XII RPL 3','alamat'=>'Bandung'],

        ];

        DB::table('siswas')->insert($sample);
    }
}
